def prime(n):
    prime_numbers = []
    not_prime_numbers = []

    '''Check for all non-prime numbers'''
    for num in range(0, n):
        if num == 1:
            pass
        else:
            for i in range(2, num):
                if num % i == 0:
                    not_prime_numbers.append(num)

    '''Find prime numbers, append to prime numbers list, and return the list'''
    for num in range(2, n):
        if num not in not_prime_numbers:
            prime_numbers.append(num)

    return prime_numbers






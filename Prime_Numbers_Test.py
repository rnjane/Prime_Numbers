import unittest

from Prime_Numbers import prime
class Prime_Numbers_Test(unittest.TestCase):
    '''Check if the method works'''
    def test_if_method_works(self):
        self.assertEquals(prime(10), [2, 3, 5, 7])

    '''Check if output of the program is a list'''
    def test_if_return_type_is_a_list(self):
        self.assertTrue(type(prime(10)) == list, msg='Output can only be a list')

    '''Check if only int is passed'''
    def test_if_input_is_int(self):
        with self.assertRaises(TypeError):
            prime('hello')

    '''Ensure no empty method input'''
    def test_if_input_is_empty(self):
        self.assertTrue(prime(10) != None)

    ''''Check if output is ok'''
    def test_if_all_prime_numbers_are_positive(self):
        numbers = prime(10)
        for i in numbers:
            self.assertTrue(i > 0)




